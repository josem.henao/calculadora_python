from fractions import Fraction

class Calculadora():

    def sumar(self, num_A, num_B):
        return Fraction(num_A + num_B)

    def restar(self, minuendo, sustraendo):
        return Fraction(minuendo - sustraendo)

    def multiplicar(self, num_A, num_B):
        return Fraction(num_A * num_B)

    def is_integer(self, numero):
        if numero == int(numero):
            return True
        else:
            return False

    def potenciar(self, numero, potencia):
        return Fraction(numero ** potencia)

    def raiz(self, numero, raiz):
        if (raiz % 2 == 0 and numero < 0) or raiz == 0:
            return None
        else:
            return Fraction(numero ** (1 / raiz))

    def dividir(self, numerador, denominador):
        if denominador == 0:
            return None
        else:
            return Fraction(numerador / denominador)

    def integer_division(self, numerador, denominador):
        return int(numerador / denominador)

    def modulo(self, numerador, denominador):
        return numerador % denominador

    def es_mayor(self, numA, numB):
        if numA > numB:
            return True
        else:
            return False

    def es_menor(self, numA, numB):
        if numA < numB:
            return True
        else:
            return False

    def es_diferente(self, numA, numB):
        if numA != numB:
            return True
        else:
            return False

    def es_mayor_igual(self, numA, numB):
        if numA >= numB:
            return True
        else:
            return False

    def es_menor_igual(self, numA, numB):
        if numA <= numB:
            return True
        else:
            return False

    def es_igual(self, numA, numB):
        if numA == numB:
            return True
        else:
            return False
