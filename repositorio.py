import mysql.connector
from settings import DB_CONFIG
from usuario import Usuario


class RepositorioUsuarios():
    conn = None

    def __init__(self):
        self.conn = mysql.connector.connect(**DB_CONFIG)

    # Registra un usuario en la base de datos --> True, else --> False
    def registrar_usuario(self, usuario):
        if self.verificar_usuario(usuario.username):
            return False
        else:
            cursor = self.conn.cursor()

            query = ("INSERT INTO usuarios "
                     "(username, password) "
                     "VALUES (%s, %s)")
            data_query = (str(usuario.username), str(usuario.password))

            cursor.execute(query, data_query)

            self.conn.commit()
            cursor.close()
            return True

    # Obtiene un usuario y en caso de estar en la BD --> Usuario, else --> None
    def obtener_usuario(self, username):
        cursor = self.conn.cursor()
        query = ("SELECT * FROM USUARIOS "
                 "WHERE USERNAME = '{}'".format(username))
        cursor.execute(query)
        row = cursor.fetchone()
        if row is not None:
            user_found = Usuario(username=row[0],password=row[1])
            return user_found
        else:
            return None

    #Elimina un usuario --> True, else --> False
    def eliminar_usuario(self, usuario):
        if self.verificar_usuario(usuario.username):
            cursor = self.conn.cursor()
            query = ("DELETE FROM USUARIOS WHERE USERNAME = '{}'".format(usuario.username))
            cursor.execute(query)
            self.conn.commit()
            cursor.close()
            return True
        else:
            return False

    #Modifica el Pasword de un usuario ingresado --> True, else --> False
    def modificar_usuario(self, usuario):
        if self.verificar_usuario(usuario.username):
            cursor = self.conn.cursor()
            query = ("UPDATE USUARIOS set password = 's%' where username = 's%'")
            data_query = (usuario.password, usuario.username)

            cursor.execute(query)
            return True
        else:
            return False

    # Verifica si un usuario está registrado en la base de datos --> True, else --> False
    def verificar_usuario(self, username):
        cursor = self.conn.cursor()

        query = ("SELECT * FROM USUARIOS "
                 "WHERE USERNAME = '{}'".format(username))

        cursor.execute(query)

        row = cursor.fetchone()
        if row is not None and row[0] == username:
            return True
        else:
            False
        self.conn.commit()
        cursor.close()

class RepositorioEventos():
    conn = None

    def __init__(self):
        self.conn = mysql.connector.connect(**DB_CONFIG)