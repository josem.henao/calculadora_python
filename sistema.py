from calculadora import Calculadora
from menu import MENU_CALCULADORA
from repositorio import RepositorioUsuarios
from usuario import Usuario

class Sistema():
    repositorio_usuarios = None
    repositorio_eventos = None
    usuario_logueado = None
    calculadora = None

    # Inicialización del sistema con el repositorio y la calculadora
    def __init__(self):
        self.calculadora = Calculadora()
        self.repositorio_usuarios = RepositorioUsuarios()

    # Método de Registro de usuario
    def registrar_usuario(self):
        username = input("Username: ")
        password = input("Password: ")
        if not self.verificar_usuario(username):
            usuario = Usuario(username=username,password='')
            usuario.set_password(password)
            return self.repositorio_usuarios.registrar_usuario(usuario)
        else:
            return False

    # Método de verificación de usuarios registrados
    def verificar_usuario(self, username):
        return self.repositorio_usuarios.verificar_usuario(username)

    # Crear el método de logueo de usuario
    def logear_usuario(self, usuario):
        if self.verificar_usuario(usuario):
            self.usuario_logueado = usuario
            return True
        else:
            return False

    def get_usuario_logueado(self):
        return self.usuario_logueado

    def show_calculadora(self):
        opcion = -1
        while opcion != 0:
            print(MENU_CALCULADORA)
            opcion = int(input("Ingresa tu opción: "))
            if opcion == 1:
                num1 = float(input('Ingrese primer numero: '))
                num2 = float(input('Ingrese segundo nùmero: '))
                print(self.calculadora.sumar(num1, num2))

            elif opcion == 2:
                num1 = float(input('Ingrese minuendo: '))
                num2 = float(input('Ingrese sustraendo: '))
                print(self.calculadora.restar(num1, num2))

            elif opcion == 3:
                num1 = float(input('Ingrese primer numero: '))
                num2 = float(input('Ingrese segundo número: '))
                print(self.calculadora.multiplicar(num1, num2))

            elif opcion == 4:
                num = float(input('Ingrese un número: '))
                is_integer = self.calculadora.is_integer(num)
                if (is_integer):
                    print("El número ", num, " es entero")
                else:
                    print("El número ", num, " es entero")

            elif opcion == 5:
                numero = float(input('Ingrese el número: '))
                potencia = float(input('Ingrese la potencia: '))

                print(self.calculadora.potenciar(numero, potencia))

            elif opcion == 6:
                numero = float(input('Ingrese el número: '))
                raiz = float(input('Ingrese la raíz: '))

                resultado = self.calculadora.raiz()

                if resultado is not None:
                    print(resultado)
                else:
                    print("ZeroDivisionError")



