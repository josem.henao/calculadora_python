from passlib.hash import pbkdf2_sha256 as hs

class Usuario():
    username = ''
    password = ''

    def __init__(self, username, password):
        self.username = username
        self.password = password

    def validate_password(self, raw_psw):
        return hs.verify(raw_psw, self.password)

    def set_password(self,raw_psw):
        self.password = hs.hash(raw_psw)

    def __str__(self):
        return self.username

